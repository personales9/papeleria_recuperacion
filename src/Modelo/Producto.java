/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.Objects;

/**
 * // Esta es la rama de rafael
 * 
 * @author estudiante
 */
public class Producto {
    private String nombre;
    private int codigo;
    private String descripcion;
    private int valor;
    private int stock;

    public Producto() {
    }

    public Producto(String nombre,int codigo, String descripcion, int valor, int stock) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.valor = valor;
        this.stock = stock;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }    
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
    
    public void venta(int a){
        
        if (this.stock-a > 5){
            System.out.println("Venta exitosa.");
            setStock(this.stock -a);            
        }else {
            System.out.println("No hay stock del producto para esa venta. \n por favor haga un pedido del producto.");            
        }
        
    }
    public void pedido(int a){        
        setStock(this.stock+a);
        System.out.println("a pedido " + a + "productos.");        
        
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.nombre);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Producto other = (Producto) obj;
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return true;
    }
    
    
}
