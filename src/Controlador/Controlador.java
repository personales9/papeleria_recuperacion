/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import Modelo.Producto;
import Servicios.Conexion;
import java.util.ArrayList;

/**
 *
 * @author Rafael Sarmiento
 */
public class Controlador {
    
    public static void guardar(Producto i){
      try{
         Connection conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement("INSERT INTO inventario (nombre, codigo, descripcion, valor, stock) "
                    + "VALUES(?, ?, ?, ?, ?)");
            consulta.setString(1, i.getNombre());
            consulta.setInt(2, i.getCodigo());
            consulta.setString(3, i.getDescripcion());
            consulta.setInt(4, i.getValor());
            consulta.setInt(5, i.getStock());
            consulta.executeUpdate();
      }catch(SQLException ex){
            
      } catch (ClassNotFoundException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    public static void eliminar(String a){
      try{
         Connection conexion = Conexion.obtener();
         PreparedStatement consulta;
         consulta = conexion.prepareStatement("DELETE FROM inventario WHERE nombre=? " );
         consulta.setString(1, a);
         consulta.executeUpdate();
      }catch(SQLException ex){
            
      } catch (ClassNotFoundException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public ArrayList<Producto> getProducto() throws SQLException, ClassNotFoundException {

        ArrayList<Producto> listaProductos = new ArrayList<>();

        try {
            
            System.out.println("Realizando consulta para obtener todos los productos de la Base de datos");
            Connection conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement("SELECT * FROM `inventario`");

            ResultSet rs = consulta.executeQuery();

            Producto j = new Producto();
            int i = 1;
            while (rs.next()) {
                System.out.println("Extrayendo producto #" + i);
                Producto eq = new Producto();
                eq.setNombre(rs.getString("nombre"));
                eq.setCodigo(rs.getInt("codigo"));
                eq.setDescripcion(rs.getString("descripcion"));
                eq.setValor(rs.getInt("valor"));
                eq.setStock(rs.getInt("stock"));          

                listaProductos.add(eq);
                i++;
            }

            System.out.println("Productos obtenidos con exito");
            

        } catch (SQLException e) {
            System.out.println("Error al obtener productos");
            throw new SQLException(e);
        }
        return listaProductos;
    }
    /**
     * 
     * @param p
     * @param a Nombre de producto
     * @param b El stock en la base de datos
     * @throws java.sql.SQLException
     */
    public static void vend(Producto p) throws SQLException{
        
        Connection conexion;
        try {
            conexion = Conexion.obtener();
            PreparedStatement consulta;
            consulta = conexion.prepareStatement("UPDATE FROM inventario SET stock=? WHERE codigo=? " );
            consulta.setInt(1, p.getStock());
            consulta.setInt(2, p.getCodigo());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         
    }

 
      
        
        
    
}
