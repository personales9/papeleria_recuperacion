/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servicios;

/**
 *
 * @author Rafael Sarmiento
 */
import java.sql.*;

public class Conexion {
    
    private static Connection cnx = null;
    
    public static Connection obtener() throws SQLException, ClassNotFoundException{
        if(cnx == null){
            try{
                Class.forName("com.mysql.jdbc.Driver");
                cnx = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/papeleria", "root", "");
            }catch(SQLException ex){
                throw new SQLException(ex);
            }catch(ClassNotFoundException e){
                throw new ClassNotFoundException(e.getMessage());
            }
        }
        
        return cnx;
    }
    
    public static void cerrar() throws SQLException{
        if(cnx != null){
            cnx.close();
        }
    }
    
}
